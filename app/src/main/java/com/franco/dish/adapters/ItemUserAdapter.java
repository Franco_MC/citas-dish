package com.franco.dish.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.franco.dish.GreetingActivity;
import com.franco.dish.R;
import com.franco.dish.Services.models.GenericResponseArray;
import com.franco.dish.Services.models.UserDataModel;
import com.franco.dish.Services.config.APIConfig;
import com.franco.dish.Services.config.APIServices;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemUserAdapter extends RecyclerView.Adapter<ItemUserAdapter.CustomViewHolder> {
    private ArrayList<UserDataModel> userDataList;
    private Context context;
    private APIConfig apiConfig;
    private APIServices apiServices;

    public ItemUserAdapter(ArrayList<UserDataModel> userDataList,
                           Context context,
                           APIConfig apiConfig,
                           APIServices apiServices) {
        this.userDataList = userDataList;
        this.context = context;
        this.apiServices = apiServices;
        this.apiConfig = apiConfig;
    }

    @NonNull
    @Override
    public ItemUserAdapter.CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_item_users, null);
        ItemUserAdapter.CustomViewHolder viewHolder = new ItemUserAdapter.CustomViewHolder(itemLayoutView);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull final CustomViewHolder holder, final int position) {
        final Integer id = userDataList.get(position).getId();
        holder.tv_name.setText(userDataList.get(position).getName());
        holder.tv_age.setText(userDataList.get(position).getAge());
        holder.tv_color.setText(userDataList.get(position).getColor());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Opciones");
                alertDialog.setMessage("Seleccionar una opcion");
                alertDialog.setPositiveButton("Saludar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(context, GreetingActivity.class);
                        intent.putExtra("name", userDataList.get(position).getName());
                        context.startActivity(intent);
                    }
                });
                alertDialog.setNegativeButton("Eliminar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sendDelete(id, position);
                    }
                });

                alertDialog.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                AlertDialog dialog = alertDialog.create();
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return userDataList.size();
    }


    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name;
        TextView tv_age;
        TextView tv_color;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_age = itemView.findViewById(R.id.tv_age);
            tv_color = itemView.findViewById(R.id.tv_color);
        }
    }

    private void sendDelete(int id, final int position){
        apiServices.doDeleteUser(id).enqueue(new Callback<GenericResponseArray>() {
            @Override
            public void onResponse(Call<GenericResponseArray> call, Response<GenericResponseArray> response) {
                if(response.isSuccessful()) {
                    userDataList.remove(position);
                    notifyItemRemoved(position);
                } else {
                    Toast.makeText(context, "Error en la eliminacion", Toast.LENGTH_LONG);
                }

            }

            @Override
            public void onFailure(Call<GenericResponseArray> call, Throwable t) {
                System.out.println("ItemUserAdapter Call Error");
            }
        });
    }

}
