package com.franco.dish;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.franco.dish.Services.config.APIConfig;
import com.franco.dish.Services.config.APIServices;
import com.franco.dish.Services.models.GenericResponseArray;
import com.franco.dish.Services.models.UserDataModel;
import com.franco.dish.adapters.ItemUserAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private Button bt_go_add_user;
    private ProgressBar pb_sharge;
    private APIConfig apiConfig;
    private APIServices apiServices;
    private RecyclerView rv_users;
    private ArrayList<UserDataModel> userDataModelArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv_users = findViewById(R.id.rv_users);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        rv_users.setLayoutManager(linearLayoutManager);

        apiConfig = new APIConfig(APIConfig.URL_webservice, "application/json");
        apiServices = APIConfig.getAPIService();

        pb_sharge = findViewById(R.id.pb_sharge);

        sendGet();

        bt_go_add_user = findViewById(R.id.bt_go_add_user);
            bt_go_add_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, AddUser.class);
                    startActivity(intent);
                }
            });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        sendGet();
    }

    private void sendGet() {
        pb_sharge.setVisibility(View.VISIBLE);
        final Gson gson = new Gson();
        apiServices.doGetUsers().enqueue(new Callback<GenericResponseArray>() {
            @Override
            public void onResponse(Call<GenericResponseArray> call, Response<GenericResponseArray> response) {
                if(response.isSuccessful()){
                    pb_sharge.setVisibility(View.INVISIBLE);
                    UserDataModel[] userDataModel = gson.fromJson(response.body().getData().toString(), UserDataModel[].class);
                    if(userDataModel.length > 0) {
                        userDataModelArrayList = new ArrayList<UserDataModel>();
                        for (int i = 0; i < userDataModel.length; i++) {
                            userDataModelArrayList.add(userDataModel[i]);
                        }
                        ItemUserAdapter itemUserAdapter = new ItemUserAdapter(userDataModelArrayList, MainActivity.this, apiConfig, apiServices);
                        rv_users.setAdapter(itemUserAdapter);
                    } else {
                        Toast.makeText(getApplicationContext(), "No existen registros", Toast.LENGTH_LONG).show();
                    }
                } else {
                        pb_sharge.setVisibility(View.INVISIBLE);
                        Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponseArray> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
