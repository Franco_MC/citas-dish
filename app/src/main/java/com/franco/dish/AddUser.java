package com.franco.dish;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.franco.dish.Services.models.GenericResponseArray;
import com.franco.dish.Services.models.UserDataModel;
import com.franco.dish.Services.config.APIConfig;
import com.franco.dish.Services.config.APIServices;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddUser extends AppCompatActivity {
    private APIConfig apiConfig;
    private APIServices apiServices;
    private UserDataModel userDataModel;
    private Button bt_add_user;
    private TextView tv_add_name;
    private TextView tv_add_age;
    private TextView tv_add_color;
    private ProgressBar pb_add;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);

        apiConfig = new APIConfig(APIConfig.URL_webservice, "application/json");
        apiServices = APIConfig.getAPIService();

        tv_add_name = findViewById(R.id.tv_add_name);
        tv_add_age = findViewById(R.id.tv_add_age);
        tv_add_color = findViewById(R.id.tv_add_color);
        pb_add = findViewById(R.id.pb_add);

        bt_add_user = findViewById(R.id.bt_add_user);
            bt_add_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userDataModel = new UserDataModel();
                    if(tv_add_name.getText().length() == 0) {
                        Toast.makeText(getApplicationContext(), "Nombre requerido", Toast.LENGTH_LONG).show();
                    } else {
                        userDataModel.setName(tv_add_name.getText().toString());
                    }

                    if(tv_add_age.getText().length() == 0) {
                        Toast.makeText(getApplicationContext(), "Edad requerida", Toast.LENGTH_LONG).show();
                    } else {
                        userDataModel.setAge(tv_add_age.getText().toString());
                    }

                    if(tv_add_color.getText().length() == 0) {
                        Toast.makeText(getApplicationContext(), "Color requerido", Toast.LENGTH_LONG).show();
                    } else {
                        userDataModel.setColor(tv_add_color.getText().toString());
                    }

                    if(userDataModel.getName() != null
                        && userDataModel.getAge() != null
                        && userDataModel.getColor() != null) {
                        sendPost(userDataModel);
                    }
                }
            });
    }

    private void sendPost(UserDataModel userDataModel){
        pb_add.setVisibility(View.VISIBLE);
        final Gson gson = new Gson();
        apiServices.doPostUser(userDataModel).enqueue(new Callback<GenericResponseArray>() {
            @Override
            public void onResponse(Call<GenericResponseArray> call, Response<GenericResponseArray> response) {
                if(response.isSuccessful()) {
                    System.out.println("AddUser isSuccess: " + response.body().getData());
                    onBackPressed();
                    pb_add.setVisibility(View.INVISIBLE);
                } else {
                    pb_add.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(), "Error al agregar el usuario", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponseArray> call, Throwable t) {
                pb_add.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
