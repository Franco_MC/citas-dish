package com.franco.dish;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.franco.dish.Services.models.GenericResponseObject;
import com.franco.dish.Services.models.JokeModel;
import com.franco.dish.Services.config.APIConfig;
import com.franco.dish.Services.config.APIServices;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GreetingActivity extends AppCompatActivity {
    private TextView tv_cita;
    private String name;
    private APIConfig apiConfig;
    private APIServices apiServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greeting);
        apiConfig = new APIConfig(APIConfig.URL_joke, "application/json");
        apiServices = APIConfig.getAPIService();

        sendGetJoke();

        Intent intent = getIntent();
        name = intent.getStringExtra("name");

        Toast.makeText(this, "Hola " + name, Toast.LENGTH_SHORT).show();
        tv_cita = findViewById(R.id.tv_cita);
    }

    private void sendGetJoke() {
        final Gson gson = new Gson();
        apiServices.doGetCita().enqueue(new Callback<GenericResponseObject>() {
            @Override
            public void onResponse(Call<GenericResponseObject> call, Response<GenericResponseObject> response) {
                if(response.isSuccessful()) {
                    JokeModel jokeModel = gson.fromJson(response.body().getValue().toString(), JokeModel.class);
                    tv_cita.setText(jokeModel.getJoke());
                } else {
                    Toast.makeText(GreetingActivity.this, "Error en la respuesta del servicio", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GenericResponseObject> call, Throwable t) {
                Toast.makeText(GreetingActivity.this, "Error en conexion " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
