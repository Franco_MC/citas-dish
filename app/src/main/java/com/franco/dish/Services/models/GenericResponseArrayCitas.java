package com.franco.dish.Services.models;

import com.google.gson.JsonArray;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenericResponseArrayCitas {
    @SerializedName("type")
    @Expose
    String type;

    @SerializedName("value")
    @Expose
    JsonArray value;
}
