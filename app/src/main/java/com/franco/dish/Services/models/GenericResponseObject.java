package com.franco.dish.Services.models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenericResponseObject {
    @SerializedName("type")
    @Expose
    String type;

    @SerializedName("value")
    @Expose
    JsonObject value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(JsonObject value) {
        this.value = value;
    }
}
