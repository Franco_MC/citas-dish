package com.franco.dish.Services.models;

import java.io.Serializable;

public class JokeModel implements Serializable {
    String id;
    String joke;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJoke() {
        return joke;
    }

    public void setJoke(String joke) {
        this.joke = joke;
    }
}
