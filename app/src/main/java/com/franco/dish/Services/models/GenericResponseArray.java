package com.franco.dish.Services.models;

import com.google.gson.JsonArray;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenericResponseArray {
    @SerializedName("code")
    @Expose
    int code;

    @SerializedName("data")
    @Expose
    JsonArray data;

    @SerializedName("message")
    @Expose
    String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public JsonArray getData() {
        return data;
    }

    public void setData(JsonArray data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
