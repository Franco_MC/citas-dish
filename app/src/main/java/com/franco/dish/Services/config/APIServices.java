package com.franco.dish.Services.config;

import com.franco.dish.Services.models.GenericResponseArray;
import com.franco.dish.Services.models.GenericResponseArrayCitas;
import com.franco.dish.Services.models.GenericResponseObject;
import com.franco.dish.Services.models.UserDataModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIServices {
    @GET("user")
    Call<GenericResponseArray> doGetUsers();

    @DELETE("user/{id}")
    Call<GenericResponseArray> doDeleteUser(@Path("id") int id);

    @POST("user")
    Call<GenericResponseArray> doPostUser(@Body UserDataModel userDataModel);

    @GET("random")
    Call<GenericResponseObject> doGetCita();

    @GET()
    Call<GenericResponseArrayCitas> doGetArrayCita();
}
