package com.franco.dish.Services.config;

public class APIConfig {
    public static String URL_webservice = "http://dishbk.herokuapp.com/api/";
    public static String URL_joke = "http://api.icndb.com/jokes/";

    public static String base_url;
    private static String headerType;
    private static boolean privateZone;

    public APIConfig(String base_url,
                     String headerType) {

        this.base_url = base_url;
        this.headerType = headerType;
    }

    public static APIServices getAPIService() {
        return RetrofitClient.getClient(base_url,
                headerType).create(APIServices.class);
    }
}
