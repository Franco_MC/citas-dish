package com.franco.greendaogenerator;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Schema;
import org.greenrobot.greendao.generator.Entity;

public class GreenGenerator {
    public static void main(String[] args) {
        Schema schema = new Schema(1, "com.franco.dish.db");
        schema.enableKeepSectionsByDefault();

        addTables(schema);

        try {
            new DaoGenerator().generateAll(schema,"./app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTables(final Schema schema) {
        addCitaEntities(schema);
    }

    private static Entity addCitaEntities(final Schema schema) {
        Entity cita = schema.addEntity("Cita");
        cita.addIdProperty().primaryKey().autoincrement();
        cita.addIntProperty("cita_id").notNull();
        cita.addStringProperty("cita");

        return cita;
    }
}

